import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import 'rxjs/add/observable/throw';

export class ErrorHandler {
    static HandlerError(error: Response | any) {
        //console.warn(error);
        return Observable.throw(error);
    }   
}