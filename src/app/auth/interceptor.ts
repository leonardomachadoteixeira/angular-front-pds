import { ErrorHandler } from './../app.error-handler';
import { API_URL } from './../app.api';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Response } from '@angular/http/src/static_response';
import { Router } from '@angular/router';
import { Jsonp } from '@angular/http';
import { parseString } from "xml2js";
import { DatePipe } from '@angular/common';

@Injectable()

export class Interceptor {

    constructor(private _http: Http, private _router: Router, private _Jsonp: Jsonp, private datePipe: DatePipe) { }


    post(url, bodyRequest) {

        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers });
        let body = new URLSearchParams(bodyRequest);

        return this._http.post(API_URL + url, body.toString(), options).map(Response => Response).catch(ErrorHandler.HandlerError);

    }

    get(url, bodyRequest) {

        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers });

        return this._http.get(API_URL + url, options).map(Response => Response).catch(ErrorHandler.HandlerError);

    }

    put(url, bodyRequest) {

        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers });
        let body = new URLSearchParams(bodyRequest);

        return this._http.put(API_URL + url, body.toString(), options).map(Response => Response).catch(ErrorHandler.HandlerError);
    }

    delete(url){
        
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers });

        return this._http.delete(API_URL + url, options).map(Response => Response).catch(ErrorHandler.HandlerError);
    }

}