import { ToastService } from './../../toast.service';
import { Interceptor } from './../../auth/interceptor';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { MaterializeAction } from "angular2-materialize";
import { Router } from '@angular/router';
import { AuthService } from 'angular4-social-login';
import { LocalDataSource } from 'ng2-smart-table';
import { NG_MODEL_WITH_FORM_CONTROL_WARNING } from '@angular/forms/src/directives';

declare var Materialize: any;
@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {

  tapTargetActions = new EventEmitter<MaterializeAction>();
  tableData: LocalDataSource = new LocalDataSource();
  modalActions1 = new EventEmitter<string | MaterializeAction>();
  modalActions2 = new EventEmitter<string | MaterializeAction>();
  radioButtonValue: any;
  rooms = [];
  params = [];
  Salas = [];
  RespSalas = [];
  settings = {
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      name: {
        title: 'Nome da sala'
      },
      description: {
        title: 'Descrição'
      },
      video: {
        title: 'Vídeo'
      }
    }
  };

  UpdateAccData = {
    email: '',
    password: '',
    confirmPassword: ''
  }

  constructor(private _toast: ToastService, private _router: Router, private authService: AuthService, private _interceptor: Interceptor) { }

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('dash-back');

  }
  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('dash-back');
  }

  loadData() {
    let data = {
      'iduser': JSON.parse(sessionStorage.getItem('idUser'))
    }
    this._interceptor.post('rooms/user', data)
      .subscribe(Response => {
        this.rooms = JSON.parse(Response._body);
        this.tableData.load(JSON.parse(Response._body));
      }, (error) => {
        console.warn(error);
      })
  }

  openTapTarget() {
    this.tapTargetActions.emit({ action: "tapTarget", params: ["open"] });
  }
  closeTapTarget() {
    this.tapTargetActions.emit({ action: "tapTarget", params: ["close"] });
  }


  onCreateConfirm(event): void {
    let data = event.newData;
    data.iduser = JSON.parse(sessionStorage.getItem('idUser'));
    this._interceptor.post('rooms/search', event.newData)
      .subscribe(Response => {
        this._interceptor.post('rooms', data)
          .subscribe(Response => {
            this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;Sala criada com sucesso!')
            event.confirm.resolve();
            this.loadData();
          }, (error) => {
            console.warn(error);
          })
      }, (error) => {
        if (error.status == 401) {
          this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;' + error._body);
        }
      });
  }

  onSaveConfirm(event): void {
    this._interceptor.put('rooms/' + event.data.id, event.newData)
      .subscribe(Response => {
        this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;Sala atualizada com sucesso!')
        event.confirm.resolve();
        this.loadData();
      }, (error) => {
        console.warn(error);
      })
  }

  onDeleteConfirm(event): void {
    this._interceptor.delete('rooms/' + event.data.id)
      .subscribe(Response => {
        this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;Sala excluida com sucesso!')
        event.confirm.resolve();
        this.loadData();
      }, (error) => {
        console.warn(error);
      })
  }

  onSignOut() {
    this.authService.signOut();
    this._router.navigate(['']);
  }

  openModal1() {
    this.modalActions1.emit({ action: "modal", params: ['open'] });
  }
  closeModal1() {
    this.modalActions1.emit({ action: "modal", params: ['close'] });
  }

  openModal2() {
    this.modalActions2.emit({ action: "modal", params: ['open'] });
  }
  closeModal2() {
    this.modalActions2.emit({ action: "modal", params: ['close'] });
  }

  goColors() {
    this.closeModal1();
    this._router.navigate(['colors'], { queryParams: { id: this.radioButtonValue } });
  }

  UpdateEmail(email) {
    let data = {
      'email': email,
      'iduser': JSON.parse(sessionStorage.getItem('idUser'))
    }
    this._interceptor.put('user/' + JSON.parse(sessionStorage.getItem('idUser')), data)
      .subscribe(Response => {
        this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;E-mail atualizado com sucesso!')
      }, (error) => {
        console.warn(error)
      })
  }

  UpdatePassword(data) {
    if (data.password != data.confirmPassword) {
      this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;Ops.. senhas se diferem!');
    } else {
      let params = {
        'password': data.password,
        'iduser': JSON.parse(sessionStorage.getItem('idUser'))
      }
      this._interceptor.put('userpassword/' + JSON.parse(sessionStorage.getItem('idUser')), params)
        .subscribe(Response => {
          this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;Senha atualizado com sucesso!')
        }, (error) => {
          console.warn(error)
        })
    }
  }

}
