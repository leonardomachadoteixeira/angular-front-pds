import { ToastService } from './../../toast.service';
import { Interceptor } from './../../auth/interceptor';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { Directive, AfterViewInit, OnDestroy } from '@angular/core';
import { AuthService } from "angular4-social-login";
import { GoogleLoginProvider } from "angular4-social-login";
import { Router } from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  modalActions1 = new EventEmitter<string | MaterializeAction>();
  modalActions2 = new EventEmitter<string | MaterializeAction>();
  params = [];
  user: any;
  loggedIn: any;
  formData = {
    username: '',
    password: ''
  }

  formDataMobile = {
    username: '',
    password: ''
  }

  newAccData = {
    username: '',
    email: '',
    password: '',
    confirmPassword: ''
  }

  resetPassword = {
    username: '',
    email: ''
  }

  constructor(private _toast: ToastService, private authService: AuthService, private _router: Router, private _interceptor: Interceptor) { }

  ngOnInit() {
    sessionStorage.clear();
    this.signOut();
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      if (this.loggedIn == true) {
        sessionStorage.setItem('LoggedIn', JSON.stringify(true));
        sessionStorage.setItem('idUser', JSON.stringify(user.id));
        this._router.navigate(['dashboard']);
      }
    });
  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('home-background');

  }
  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('home-background');
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  openModal1() {
    this.modalActions1.emit({ action: "modal", params: ['open'] });
  }
  closeModal1() {
    this.modalActions1.emit({ action: "modal", params: ['close'] });
  }

  openModal2() {
    this.modalActions2.emit({ action: "modal", params: ['open'] });
  }
  closeModal2() {
    this.modalActions2.emit({ action: "modal", params: ['close'] });
  }

  signOut(): void {
    this.authService.signOut();
  }

  tryLogin(data) {
    this._interceptor.post('login', data)
      .subscribe(Response => {
        let dataResp = JSON.parse(Response._body);
        console.warn(Response.status);
        if (Response.status == 200) {
          sessionStorage.setItem('LoggedIn', JSON.stringify(true));
          sessionStorage.setItem('idUser', JSON.stringify(dataResp.id));
          this._router.navigate(['dashboard']);
        }
      }, (error) => {
        this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;' + error._body);
      });
  }

  createAccount(data) {
    if (data.password != data.confirmPassword) {
      this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;Ops.. senhas se diferem!');
    } else {
      this._interceptor.post('user', data)
        .subscribe(Response => {
          if (Response.status == 201) {
            this.closeModal1();
            this.tryLogin(data);
          }
        }, (error) => {
          this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;' + error._body);
        });
    }
  }

  onBlurMethod(data) {
    this._interceptor.post('searchUser', data)
      .subscribe(Response => {
        console.warn('Ok');
      }, (error) => {
        if (error.status == 401) {
          this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;' + error._body);
          this.newAccData.username = '';
        }
      });
  }

  ResetPasswordFunc(data) {
    this._interceptor.post('user/recover', data)
      .subscribe(Response => {
        this._toast.toast('<i class="material-icons green-text">done</i>&nbsp;&nbsp;Siga as instruções enviadas para '+ data.email +'!');
      }, (error) => {
        this._toast.toast('<i class="material-icons red-text">error_outline</i>&nbsp;&nbsp;' + error._body);
      });
  }



}
