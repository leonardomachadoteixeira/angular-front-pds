import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToastService } from '../../toast.service';
import { Interceptor } from '../../auth/interceptor';
import { DomSanitizer } from '@angular/platform-browser';

declare var require: any;


require('../../../assets/runtime-script');

const Ws = require('@adonisjs/websocket-client');
let ws = null;
let isConnected = false;

ws = Ws('ws://pds-leonardo-com.umbler.net').connect();
//ws = Ws('ws://192.168.0.103:3333');

ws.on('open', (state) => {
  isConnected = true
})

ws.on('close', () => {
  isConnected = false
})

let jpm;

let cur = 'black';
let low: number = 10;
let medium: number = 10;
let high: number = 10;



@Component({
  selector: 'app-colors-page',
  templateUrl: './colors-page.component.html',
  styleUrls: ['./colors-page.component.css']
})
export class ColorsPageComponent implements OnInit {

  currence = 'black';
  id: any;
  room: any;
  idvideo = '';
  imgSrc: any;
  bol: boolean = false;
  constructor(public zone: NgZone, public sanitizer: DomSanitizer, private route: ActivatedRoute, private _toast: ToastService, private _interceptor: Interceptor) {

  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this._interceptor.get('rooms/' + this.id, null)
        .subscribe(Response => {
          this.room = JSON.parse(Response._body);
          if (ws.getSubscription('jpm:' + this.room.name.toUpperCase()) != null) {
            jpm = ws.getSubscription('jpm:' + this.room.name.toUpperCase());
          } else {
            jpm = ws.subscribe('jpm:' + this.room.name.toUpperCase());
          }
          
          jpm.on('message', (response) => {

            let color = '';
            if (response > 0 && response <= low) {
              color = 'black';
            } else if (response > low && response <= medium) {
              color = 'blue';
            } else if (response > medium && response <= high) {
              color = 'orange';
            } else if (response > high) {
              color = 'red';
            }

            if (cur != color) {
              cur = color;
            }

          })


          jpm.on('average', (response) => {
            //console.warn('average');
            low = response - 10 | 0;
            medium = response + 10 | 0;
            high = response + 25 | 0;
          })
          this.room.video = this.room.video.replace("https://www.youtube.com/watch?v=", "");
          this.zone.run(() => this.idvideo = this.room.video);
          this.bol = true;
        }, (error) => {
          console.warn(error);
        })
    });

    setInterval(() => {
      if (cur != this.currence) {
        document.querySelector('#boxcolor').classList.remove(this.currence);
        document.querySelector('#boxcolor').classList.add(cur);
        this.currence = cur;
      }
    }, 500);

  }

  ngAfterViewInit() {
    document.querySelector('body').classList.add('no-scroll');
    document.querySelector('body').classList.add('back-color');
    cur = 'black';
    document.querySelector('#boxcolor').classList.add('black');
  }
  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('no-scroll');
    document.querySelector('body').classList.remove('back-color');
    jpm.close();
  }


}
