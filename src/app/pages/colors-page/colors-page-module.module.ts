import { Routes, RouterModule } from '@angular/router';
import { ColorsPageComponent } from './colors-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YoutubePlayerModule } from 'ng2-youtube-player';

const routes: Routes = [
  { path: '', component: ColorsPageComponent }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    YoutubePlayerModule
    
  ],
  declarations: [ColorsPageComponent]
})
export class ColorsPageModuleModule { }
