import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';


export const ROUTES: Routes = [

    { path: '', redirectTo: 'home', pathMatch: 'full' },

    { path: 'home', component: HomePageComponent },

    { path: 'colors', loadChildren: './pages/colors-page/colors-page-module.module#ColorsPageModuleModule', canActivate: [AuthGuard] },

    { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard] }
]

