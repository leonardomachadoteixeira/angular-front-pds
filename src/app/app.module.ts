import { AuthGuard } from './auth/auth.guard';
import { DatePipe } from '@angular/common';
import { HttpModule, JsonpModule } from '@angular/http';
import { Interceptor } from './auth/interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ROUTES } from './app.routs';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { RouterModule } from '@angular/router';
import { SocialLoginModule, AuthServiceConfig } from "angular4-social-login";
import { GoogleLoginProvider } from "angular4-social-login";
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';
import { ToastService } from './toast.service';
import { YoutubePlayerModule } from 'ng2-youtube-player';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { CommonModule } from '@angular/common';

let config = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider("801000710865-nhpkiafqi7melj1rao9esjhgksc0jt06.apps.googleusercontent.com")
  }
]);

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    DashboardPageComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    CommonModule,
    YoutubePlayerModule,
    Ng2SmartTableModule,
    FormsModule,
    MaterializeModule,
    JsonpModule,
    HttpModule,
    RouterModule.forRoot(ROUTES),
    SocialLoginModule.initialize(config)
  ],
  providers: [Interceptor, DatePipe, ToastService, AuthGuard, LocalDataSource],
  bootstrap: [AppComponent]
})
export class AppModule { }
